var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.browserify('Admin.js', 'public/js/admin.js');
    mix.browserify('FindDomain.js', 'public/js/screen/find-domain.js');
    mix.browserify('Banner.js', 'public/js/screen/banner.js');

    mix.sass('app.sass');
    mix.sass('banner.sass', 'resources/views/banner/banner.css');
});
