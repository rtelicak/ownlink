<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
use Illuminate\Http\Request;

Route::group(['domain' => config('app.url')], function () {
	get('/', 'PagesController@home');
	get('find-your-domain', 'PagesController@findDomain');
	get('pay-for-it', ['middleware' => 'auth', 'uses' => 'PaymentsController@enter']);
	post('pay-for-it', ['middleware' => 'auth', 'uses' => 'PaymentsController@order']);

	Route::group(['prefix' => 'svc'], function() {
		get('suggest-domain', 'DomainFindController@suggest');
		get('check-domain', 'DomainFindController@check');
		get('remember-domain', 'DomainFindController@remember');
	});

	get('test', 'TestController@test');
	get('{domainName}{whatever}', ['middleware' => ['auth', 'userOwnsDomain'], 'uses' => 'Admin\DomainController@showBySlug'])
		->where([
			'domainName' => '^[a-z0-9]+\.[a-z.0-9]+$',
			'whatever' => '.*'
		]);
	// get('domains/{id}', ['middleware' => 'userOwnsDomain', 'uses' => 'DomainController@show']);

	// Authentication routes...
	get('auth/login', 'Auth\AuthController@getLogin');
	post('auth/login', 'Auth\AuthController@postLogin');
	get('auth/logout', 'Auth\AuthController@getLogout');

	// Registration routes...
	get('auth/register', 'Auth\AuthController@getRegister');
	post('auth/register', 'Auth\AuthController@postRegister');

	// Admin routes ...
	Route::group(['prefix' => 'admin', 'namespace' => 'Admin', 'middleware' => 'auth'], function() {
		get('/', 'DomainController@all'); // redirected after login
		get('domains', 'DomainController@all');
		// get('domains/{id}', ['middleware' => 'userOwnsDomain', 'uses' => 'DomainController@show']);

		get('banner/create/{id}', ['middleware' => 'userOwnsDomain', 'uses' => 'BannerController@create']);

		// ajax calls
		Route::group(['prefix' => 'svc'], function(){
			// refactor other 'svc' calls, add middleware 'ownsLink'
			get('link-shared/{type}/{linkId}', ['uses' => 'LinkController@linkWasShared']);
			get('domains/{id}/links/paginated', ['middleware' => 'userOwnsDomain', 'uses' => 'LinkController@paginated']);
			get('links/{id}', ['uses' => 'LinkController@find']);
			get('links/{id}/togglePin', ['uses' => 'LinkController@togglePin']);
			get('tagList', ['uses' => 'LinkController@tagList']);
			post('links', ['uses' => 'LinkController@create']);
			post('syncTags', ['uses' => 'LinkController@syncTags']);
			delete('links/{id}', ['uses' => 'LinkController@delete']);
		});
	});
});

Route::group(['namespace' => 'Admin'], function(){
	Route::post('/svc/link-visit', ['uses' => 'BannerController@bannerVisit']);
	Route::get('/banner/{id}', ['uses' => 'BannerController@banner']);
});

Route::get('{whatever}', ['uses' => 'RequestController@handle'])->where('whatever', '.*');
