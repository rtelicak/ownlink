<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Http\Requests;
use App\Ownlink\BannerBuilder;
use App\Jobs\AnalyzeLinkClick;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestController extends Controller {

	public function handle($uri) {
		// refactor to middleware
		if ($this->isRefererBlacklisted()) {
			abort(404);
		}

		$link = $this->resolveLink();

		$this->dispatch(new AnalyzeLinkClick($_SERVER, $link->id));

		if (! $this->urlExists($link->original)) {
			return "Site {$link->original} not found";
		}
		// this is a redirect link
		if ($link->is_redirect) return redirect($link->original, 301);

		return (new BannerBuilder($link))->bannerPage();
	}

	/**
	 * Associate incoming request with correct domain and link
	 */
	private function resolveLink(){
		$serverName = $_SERVER['HTTP_HOST'];
		$customLink = substr($_SERVER['REQUEST_URI'], 1); // remove forward slash

		\Log::info('server name: ');
		\Log::info($serverName);

		$link = \App\Link::with(['domain' => function($query) use ($serverName) {
			$query->whereName($serverName);
		}])
		->withTrashed()
		->whereCustom($customLink)->first();

		if (is_null($link)) {
			return abort(404);
		}

		return $link;
	}

	/**
	 * Check if such url exists
	 */
	private function urlExists($url){
		if(empty($url)) {
			throw new \Exception("No url provided");
		}

		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_TIMEOUT, 5);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$data = curl_exec($ch);
		$httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

		curl_close($ch);

		return $httpcode >= 200 && $httpcode < 400;
	}

	/**
	 * Preventing referer spam
	 */
	private function isRefererBlacklisted(){
		$refererUrl = array_key_exists('HTTP_REFERER', $_SERVER) ? $_SERVER['HTTP_REFERER'] : '';

		$refererBlacklist = ['http://www.facebook.com/Gertruda'];

		if (in_array($refererUrl, $refererBlacklist)) {
			return true;
		}

		return false;
	}

}
