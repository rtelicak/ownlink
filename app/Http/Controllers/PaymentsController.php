<?php

namespace App\Http\Controllers;

use App\Order;
use App\Domain;
use App\Ownlink\Websupport\DomainApi;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PaymentsController extends Controller {

    public function enter() {
        $domain = session('domain');

        return view('pages.payment', compact('domain'));
    }

    public function order(DomainApi $domainApi) {
        // TOOD: customer pay us first, then order domain
        // ....
        // $domain = Domain::find(1);
        // die(var_dump(get_class_methods($domain->order())));

        $domain = session('domain');
        $name = "{$domain['name']}.{$domain['tld']}";

        // validate domain via api
        $domainApi->validateDomain($name);

        // ordering domain
        $response = $domainApi->orderDomain($name);

        $domain = \Auth::user()->assignDomain([
            'name' => $name,
            'ws_order_id' => $response['item']['id'],
        ]);

        // save response data
        $domain->order()->save(new Order(['data' => $response]));

        return redirect('admin/domains');
    }
}
