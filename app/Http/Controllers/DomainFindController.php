<?php

namespace App\Http\Controllers;

use App\Domain;
use App\Ownlink\Utilities\Tld;
use App\Ownlink\Utilities\DomainCheck;
use HelgeSverre\DomainAvailability\AvailabilityService;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DomainFindController extends Controller
{

	public function suggest(Request $request) {
		return $this->populateSuggestions($request->term);
	}

	public function check(Request $request){
		$name = $request->get('name');
		$result = (new DomainCheck)->isAvailable($name);

		return response()->json($result);
	}

	public function remember(Request $request) {
		session(['domain' => [
			'name' => $request->get('domain'),
			'tld' => $request->get('tld')
		]]);

		return 'ok';

		// die(var_dump($request->all()));
	}

	private function populateSuggestions($companyName){
		$companyName = str_replace([' ', '	'], '', strtolower(trim($companyName)));

		$tld = '';
		if (Domain::hasTld($companyName)) {
			$tld = Domain::getTld($companyName);
			$tld = Tld::find($tld);
			$companyName = substr($companyName, 0, strpos($companyName, '.'));
		}

		// todo resolve this, to remove duplicate tld item, if one already provided


		if (is_array($tld)) {
			$allTlds = Tld::allExcept($tld['name']);
			array_unshift($allTlds, $tld);
		} else {
			$allTlds = Tld::all();
		}

		// $newName = str_replace(['a', 'e', 'i', 'o', 'u'], '', $companyName);

		// $companyName = strlen($newName) < 4 ? $companyName : $newName;

		// $s = new AvailabilityService(true);

		$results = array_map(function($tld) use ($companyName){
			return [
				'domain' => "{$companyName}",
				'tld' => "{$tld['name']}",
				'price' => "{$tld['priceWithVat']} €",
			];
		}, $allTlds);

		return response()->json($results);
	}

}
