<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Link;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateLinkRequest;
use Symfony\Component\DomCrawler\Crawler;

class LinkController extends Controller {

	use \App\Ownlink\Traits\ResponseTrait;

	/**
	 * Create new Link (CTA or Redirect)
	 */
	public function create(Request $request, CreateLinkRequest $createLinkValidator) {
		if (! $request->get('is_redirect') && $this->isProtectedAgainstXFrame($request->get('original'))) {
			return $this->fieldValidationFailed([
				'original' => ["Can't use {$request->get('original')} link."]
			]);
		}

		if (Link::withTrashed()->whereCustom($request->get('custom'))->count()) {
			return $this->fieldValidationFailed([
				'original' => ["Custom link must be unique."]
			]);
		}

		$input = array_merge($request->all(), [
			'title' => $this->getPageTitle($request->get('original')),
		]);

		$link = Link::create($input);

		// create default stats
		if (! $request->get('is_redirect')) {
			$link->stats()->create([
				'bounce_count' => 0,
				'time_spent_count' => 0,
				'conversion_count' => 0
			]);
		}

		return $this->respond([
			'message' => "Link successfuly created.",
			'link' => $link
		]);
	}

	/**
	 * Soft delete link
	 */
	public function delete(Request $request, $id) {
		$link = Link::find($id);

		if ($link->delete()) {
			return $this->respond([
				'message' => "Link successfuly unfollowed.",
				'link' => $link
			]);
		}

		return $this->respondBadRequest("Something bad happend. Try later.");
	}

	public function paginated($domainId) {
		return (new Link)->getPaginated($domainId);
	}

	/**
	 * Link was shared on social media
	 */
	public function linkWasShared($type, $linkId) {
		if (! isset($type, $linkId)) {
			throw new \InvalidArgumentException("Missing type or link id");
		}

		$link = Link::findOrFail($linkId);

		$newTagId = \App\Tag::whereType($type)->first()->id;
		$tagIds = $link->tags->pluck('id')->toArray();

		$tagIds[] = $newTagId;
		$link->tags()->sync(array_unique($tagIds));

		return 'Awesome';
	}

	/**
	 * Fetch link by id
	 */
	public function find($id) {
		$from = (new Carbon)->subWeeks(2);
		$to = new Carbon;

		return Link::with(['clicks', 'tags', 'stats'])->findOrFail($id)->populateChartData($from, $to);
	}

	/**
	 * Sync link tags
	 */
	public function syncTags(Request $request) {
		$this->validate($request, [
			'linkId' => 'required',
			'tags' => 'array'
		]);

		$link = Link::findOrFail($request->get('linkId'));

		$link->tags()->sync($request->get('tags', []));

		return Link::with('clicks', 'tags')->findOrFail($request->get('linkId'));
	}

	public function tagList() {
		return \App\Tag::all();
	}

	public function togglePin($id) {
		$link = Link::findOrFail($id);
		$link->setAttribute('is_pinned', !$link->getAttribute('is_pinned'));
		$link->save();

		return $this->respond([
			'link' => $link,
			'message' => $link->getAttribute('is_pinned') ? "Link successfuly pinned" : "Link successfuly unpinned",
		]);
	}


	/**
	 * Fetch html title by url
	 */
	private function getPageTitle($url){
		try {
			$c = new Crawler(file_get_contents($url));
			$title = $c->filter('title')->text();

			return $title;
		} catch (\Exception $e) {
			return '';
		}
	}

	/**
	 * Does page have header to prevent it to be set in iframe
	 */
	private function isProtectedAgainstXFrame($url){
		foreach (get_headers($url) as $header) {
			if (strpos($header, 'X-Frame-Options') !== false ) return true;
		}

		return false;
	}

}
