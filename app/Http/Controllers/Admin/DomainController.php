<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Carbon\Carbon;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class DomainController extends Controller {

	public function all() {
		$domains = \Auth::user()->domains()->with('links')->get();

		if ($domains->count() == 1) {
			return redirect("{$domains->first()->name}");
		}

		return view('admin.domain.all', compact('domains'));
	}

	public function showBySlug($domainName) {
		$domainId = \App\Domain::whereName($domainName)->first()->id;

		return $this->show($domainId);
	}

	public function show($id) {
		$domain = $this->getDomainData($id);

		// return $domain;

		\JavaScript::put(['domainData' => $domain]);

		// return view('admin.domain.show', compact('domain'));
		return view('admin.index', compact('domain'));
	}

	protected function getDomainData($id) {
		$domain = (new \App\Domain)->withLinkData($id);
		$domain->setAttribute('links',
			(new \App\Link)->getPaginated($id, $domain->average_click_count)
		);

		return $domain;
	}
}
