<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\LinkStats;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class BannerController extends Controller {

	public function create(Request $request) {
		$domainData = \App\Domain::findOrFail($request->id)->toArray();

		return view('admin.banner.create', compact('domainData'));
	}

	/**
	 * Make a banner if link is not redirect
	 */
	public function banner($id) {
		if (!is_numeric($id)) return;

		$link = \App\Link::with('domain')->withTrashed()->find($id);

		return view('banner.banner', compact('link'));
	}

	/**
	 * User has left page with banner on it
	 */
	public function bannerVisit(Request $request) {
		$this->validate($request, [
			'link_id' => 'required',
			'time' => 'required'
		]);

		logger($request);

		$linkStats = LinkStats::firstOrNew(['link_id' => $request->link_id]);

		$linkStats->fill([
			'conversion_count' => (int)$linkStats->conversion_count + $request->conversion,
			'time_spent_count' => (int)$linkStats->time_spent_count + $request->time,
		]);

		if ($request->time <= 10) {
			logger('increasing bounce count');
			$linkStats->bounce_count = (int)$linkStats->bounce_count + 1;
		}

		$linkStats->save();
		return 'ok';
	}
}
