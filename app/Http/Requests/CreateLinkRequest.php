<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateLinkRequest extends Request
{
	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return \Auth::check();
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'original' => 'required',
			'custom' => 'required',
			'domain_id' => 'required',
			'banner_message' => 'required_if:is_redirect,0',
			'button_text' => 'required_if:is_redirect,0',
			'button_target' => 'required_if:is_redirect,0'
		];
	}

	public function messages() {
		return [
			'original.required' => 'Original link is required',
			'custom.required' => 'Your custom link is required',
			'banner_message.required_if' => 'Banner message is required',
			'button_text.required_if' => 'Banner button text is required',
			'button_target.required_if' => 'Banner URL is required'
		];
	}
}
