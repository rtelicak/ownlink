<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class UserOwnsDomain {
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // `id` or `domainName` can be passed in request
        $domainId = is_null($request->domainName) ?
            $request->id :
            \App\Domain::whereName($request->domainName)->first()->id;

        if ($domainId && Auth::user()->ownsDomain($domainId)) {
            return $next($request);
        }

        // TODO: change to 404
        return redirect('/admin/domains');
    }
}
