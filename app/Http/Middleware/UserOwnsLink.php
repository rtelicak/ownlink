<?php

namespace App\Http\Middleware;

use Closure;

class UserOwnsLink
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {
            $redirect = \App\Link::with('domain')->findOrFail($request->id);

            if (\Auth::user()->id == $redirect->domain->user_id) return $next($request);
        } catch (\Exception $e) {
            return redirect('admin/domains');
        }

        return redirect('admin/domains');
    }
}
