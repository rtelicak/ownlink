<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Link extends Model {

	use \Illuminate\Database\Eloquent\SoftDeletes;

	protected $fillable = ['title','original', 'custom', 'note', 'domain_id', 'is_redirect', 'button_text', 'button_target', 'banner_message', 'is_pinned'];

	protected $table = 'links';

	public function domain() {
		return $this->belongsTo('App\Domain');
	}

	public function clicks() {
		return $this->hasMany('App\Click')->noBots();
	}

	public function tags() {
		return $this->belongsToMany('App\Tag', 'link_tag');
	}

	public function stats() {
		return $this->hasOne('App\LinkStats');
	}

	public function setIsRedirectAttribute($value) {
		$this->attributes['is_redirect'] = $value ? 1 : 0;
	}

	protected $casts = [
		'is_redirect' => 'boolean',
		'is_pinned' => 'boolean',
	];

	public function getButtonTargetAttribute($url) {
		$utmParams = 'utm_campaign=ownlink&utm_medium=ownlink&utm_source=ownlink';
		$separator = (parse_url($url, PHP_URL_QUERY) == NULL) ? '?' : '&';

		return "{$url}{$separator}{$utmParams}";
	}

	public function getPaginated($domainId, $avgClickCount = null) {
		if (is_null($avgClickCount)) {
			$avgClickCount = (new \App\Domain)->withLinkData($domainId)->average_click_count;
		}

		$links = Link::with('clicks', 'tags', 'stats')->whereDomainId($domainId)
					 ->orderBy('is_pinned', 'desc')
					 ->orderBy('created_at', 'desc')
					 ->paginate(8);

		$links->each(function($link) use ($avgClickCount){
			$link->populateChartData();
			$link->setAttribute('click_rate', $avgClickCount > 0 ?
				round(($link->clicks->count() / $avgClickCount) * 100 - 100)
				: 0);
		});

		return $links->toArray();
	}

	public function populateChartData(Carbon $from = null, Carbon $to = null) {
		if (is_null($from)) $from = (new Carbon)->subWeeks(2);
		if (is_null($to)) $to = new Carbon;


		if(empty($this->clicks)) {
			return $this->setAttribute('chart_data', []);
		}

		$clicks = $this->clicks
			->filter(function($click) use ($from) {
				return $click->created_at > $from;
			})
			->sortBy(function($click){
				return $click->created_at;
			})->groupBy(function($click){
				return $click->created_at->format('j.n.');
			});

		$return = [];

		foreach ($clicks->toArray() as $day => $clicks) {
			$return[$day] = count($clicks);
		}

		$this->setAttribute('chart_data', array_merge($this->getEmptyPeriodMap($from), $return));

		return $this;
	}

	private function getEmptyPeriodMap(Carbon $from){
		$now = new Carbon;
		$emptyMonth = [];

		do {
			$emptyMonth[$from->format('j.n.')] = 0;
		} while ($from->addDay()->lte($now));

		return $emptyMonth;
	}
}
