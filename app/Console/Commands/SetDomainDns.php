<?php

namespace App\Console\Commands;


use App\Domain;
use App\Ownlink\Websupport\DomainApi;
use Illuminate\Console\Command;

class SetDomainDns extends Command
{

	protected $domainApi;

	protected $signature = 'domain:set-dns';

	protected $description = 'Set DNS A record to purchased domain';

	public function __construct() {
		$this->domainApi = new DomainApi;

		parent::__construct();
	}

	public function handle() {
		// find domains with not set dns server
		$collection = Domain::whereNotNull('ws_order_id')
						 ->whereIsDnsSet(0)
						 ->get();

		if ($collection->isEmpty()) return;

		// try to set correct A Record to each domain
		$collection->each(function($domain){
			$domainName = $domain->getAttribute('name');

			$record = $this->domainApi->getDomainARecord($domainName);

			$response = $this->domainApi->updateARecord($record['id'], $domainName);

			$domain->setAttribute('is_dns_set', 1);
			$domain->save();
		});
	}
}
