<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkStats extends Model {
    protected $table = 'link_stats';

    protected $fillable = ['link_id', 'bounce_rate', 'time_spent_count', 'conversion_count'];
}
