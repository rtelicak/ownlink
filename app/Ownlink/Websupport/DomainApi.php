<?php

namespace App\Ownlink\Websupport;

use websupport\RestConnection;
use App\Ownlink\Exceptions\DomainApiException;

class DomainApi {

	// IP of digital ocean VPS of mine
	protected $ip = '46.101.205.64';

	protected $connection;

	function __construct() {
		$this->connection = new RestConnection('https://rest.websupport.sk/v1/', 'roman.telicak.sk', 'ro66te');
	}

	/**
	 * Get info about current user
	 */
	public function getSelfInfo() {
		return $this->get('user/self');
	}

	/**
	 * Validate domain against API
	 */
	public function validateDomain($domain) {
		return $this->post('order/sk/validate/domain', ['domain' => $domain]);
	}

	/**
	 * Place an order for domain
	 */
	public function orderDomain($domain) {
		$order = ['services' => [
			['type' => 'domain', 'domain' => $domain]
		]];

		// $response = $this->post('user/self/order', $order);
		return $this->post('user/self/order?dryRun=1', $order);
	}

	/**
	 * Get A Record of purchased domain
	 */
	public function getDomainARecord($domain) {
		$records = $this->getRecordsForDomain($domain);

		foreach ($records['items'] as $record) {
			if ($record['type'] == 'A' && $record['name'] == '@') {
				return $record;
			}
		}

		throw new \Exception("A record for {$domain} not found", 1);
	}

	/**
	 * Update domain's A record to point to my VPS
	 */
	public function updateARecord($recordId, $domainName) {
		// PUT https://rest.websupport.sk/v1/user/:id/zone/:domain_name/record/:id HTTP/1.1
		return $this->put("user/self/zone/{$domainName}/record/{$recordId}", [
			'name' => '@',
			'content' => $this->ip,
			'ttl' => 600
		]);
	}

	/**
	 * Get all Records for purchased domain
	 */
	private function getRecordsForDomain($domain) {
		return $this->get("user/self/zone/{$domain}/record");
	}

	/**
	 * Wrappers around API  methods
	 */
	private function get($url){
		$response = $this->connection->get($url);

		return object_to_array($response);
	}

	private function post($path, $arguments){
		return $this->callApi('post', $path, $arguments);
	}

	private function put($path, $arguments){
		return $this->callApi('put', $path, $arguments);
	}

	private function callApi($method, $path, $arguments){
		$response = $this->connection->{$method}($path, $arguments);
		$response = object_to_array($response);

		if ($response['status'] != 'success') {
			throw new DomainApiException("{$method}: {$path} failed", $response);
		}

		return $response;
	}
}