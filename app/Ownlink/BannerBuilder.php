<?php

namespace App\Ownlink;

use Symfony\Component\DomCrawler\Crawler;

class BannerBuilder {

	protected $link;

	function __construct(\App\Link $link) {
		$this->link = $link;
	}

	public function bannerPage() {
		$html = $this->fetchHtmlWithFakeUA($this->link->original);

		if ($html) {
			$crawler = new Crawler($html);
			$htmlHead = $crawler->filter('head')->html();
		}

		return view('banner.envelope', [
			'link' => $this->link,
			'htmlHead' => empty($htmlHead) ? '' : $htmlHead
		]);
	}

	protected function fetchHtmlWithFakeUA($url) {
		$opts = array(
			'http'=>array(
				'header'=> "user-agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10_10_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.90 Safari/537.36"
				)
			);

		$context = stream_context_create($opts);

		try {
			return file_get_contents($url, false, $context);
		} catch (\ErrorException $e) {
			return;
		}
	}
}