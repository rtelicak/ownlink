<?php

namespace App\Ownlink\Traits;

trait ResponseTrait {

	protected $statusCode = 200;

	public function setStatusCode($code) {
		$this->statusCode = $code;

		return $this;
	}

	public function getStatusCode() {
		return $this->statusCode;
	}

	public function respond($data) {
		return response()->json($data, $this->getStatusCode());
	}

	public function respondBadRequest($message) {
		return $this->setStatusCode(400)->respond([
			'message' => $message,
			'error_code' => $this->getStatusCode()
		]);;
	}

	public function fieldValidationFailed($data) {
		return $this->setStatusCode(422)->respond($data);
	}
}