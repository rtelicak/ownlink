<?php

namespace App\Ownlink\Exceptions;

class DomainApiException extends \Exception {

	protected $apiResponseData;

	public function __construct($message, $apiResponseData) {
		$this->apiResponseData = $apiResponseData;

		parent::__construct($message, 0, null);
	}

	public function getApiResponse() {
		return $this->apiResponseData;
	}
}