<?php

namespace App\Ownlink\Detectors;

class GeoDetector {

	protected $baseUrl = 'http://ip-api.com/php/';

	protected $server;

	function __construct($server) {
		$this->server = $server;
	}

	public function getData() {
		$data = $this->getLocation();

		if (empty($data)) return [];

		return [
			'city' => $data['city'],
			'country' => $data['country'],
			'country_code' => $data['countryCode']
		];
	}

	public function getLocation() {
		$url = $this->buildRestUrl();

		$response = unserialize(file_get_contents($url));

		if (! $this->responseOk($response)) return [];

		return $response;
	}

	private function buildRestUrl(){
		return $this->baseUrl . $this->server['REMOTE_ADDR'];
	}

	private function responseOk($response){
		return array_key_exists('status', $response) && $response['status'] == 'success';
	}
}