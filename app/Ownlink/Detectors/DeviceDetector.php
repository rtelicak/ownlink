<?php

namespace App\Ownlink\Detectors;

use Agent;


class DeviceDetector {

	function __construct($server) {
		// $userAgent = 'BlackBerry7100i/4.1.0 Profile/MIDP-2.0 Configuration/CLDC-1.1 VendorID/103';
		Agent::setUserAgent(array_key_exists('HTTP_USER_AGENT', $server) ? $server['HTTP_USER_AGENT'] : '');
		Agent::setHttpHeaders($this->getHeaders($server));
	}

	public function getData() {
		$platform = Agent::platform();
		$browser = Agent::browser();

		return [
			'platform' => $platform,
			'platform_version' => Agent::version($platform),
			'browser' => $browser,
			'browser_version' => Agent::version($browser),
			'device' => Agent::device(),
			'is_desktop' => Agent::isDesktop(),
			'is_mobile' => Agent::isMobile(),
			'is_tablet' => Agent::isTablet()
		];
	}

	private function getHeaders($server){
		$headers = [];

		foreach ($server as $key => $value) {
			if (preg_match('/HTTP_([a-zA-Z]+)/', $key, $matches)){
				$headers[strtolower($matches[1])] = $value;
			}
		}

		return $headers;
	}

}