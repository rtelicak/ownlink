<?php

use Snowplow\RefererParser\Parser;

namespace App\Ownlink\Detectors;

class RefererDetector {

	protected $server;

	function __construct($server) {
		$this->server = $server;
	}

	public function getData() {
		$refererUrl = array_key_exists('HTTP_REFERER', $this->server) ? $this->server['HTTP_REFERER'] : '';

		if (! $refererUrl) return [];

		$referer = (new \Snowplow\RefererParser\Parser)->parse($refererUrl);


		if ($referer->isKnown()) {
			return [
				'referer_url' => $refererUrl,
				'medium' => $referer->getMedium(),
				'source' => $referer->getSource(),
				'search_term' => $referer->getSearchTerm(),
			];
		}

		return ['referer_url' => $refererUrl];
	}

}