<?php

namespace App\Ownlink\GoDaddy;

use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;

class GoDaddy {

	protected $url;

	protected $requestOptions = [
			'http'=>[
				'method'=> "",
				'header'=> "Accept-language: en\r\n" .
						   "Accept: application/json\r\n"
		]];

	function __construct() {
		$this->setAuthorizationHeader();
	}

	public function suggestDomains($domain) {
		return $this->setUrl("https://api.godaddy.com/v1/domains/suggest?query={$domain}&sources=CC_TLD")
					->setRequestMethod('GET')
					->callApi();
	}

	public function checkDomain($domain) {
		// https://api.ote-godaddy.com/v1/domains/available?domain=kajsdklasjdasd.com&forTransfer=false&checkType=FAST&
		return $this->setUrl("https://api.godaddy.com/v1/domains/available?domain={$domain}&checkType=FAST")
					->setRequestMethod('GET')
					->callApi();
	}

	private function callApi(){
		$data = file_get_contents($this->url, false, stream_context_create($this->requestOptions));

		return json_decode($data);
	}

	private function setUrl($url){
		$this->url = $url;

		return $this;
	}

	private function setRequestMethod($method){
		$this->requestOptions['http']['method'] = $method;

		return $this;
	}

	private function setAuthorizationHeader(){
		$this->requestOptions['http']['header'] .= "Authorization: sso-key ".env('GO_DADDY_PUBLIC_KEY').":".env('GO_DADDY_SECRET_KEY')."\r\n";
	}
}