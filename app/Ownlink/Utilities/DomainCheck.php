<?php

namespace App\Ownlink\Utilities;


class DomainCheck {

	public function isAvailable($domain) {
		$data = file_get_contents("https://frm.websupport.sk/frm/api_domaincheck.php/{$domain}");

		return json_decode($data);
	}

}
