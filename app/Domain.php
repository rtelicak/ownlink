<?php

namespace App;

use Cache;
use Illuminate\Database\Eloquent\Model;

class Domain extends Model {

	protected $table = 'domains';

	protected $fillable = ['name', 'ws_order_id', 'a_record_id', 'is_active', 'config'];

	protected $casts = [
		'config' => 'array'
	];

	public function links() {
		return $this->hasMany('App\Link');
	}

	public function user() {
		return $this->belongsTo('App\User');
	}

	public function order() {
		return $this->hasOne('App\Order');
	}

	public static function hasTld($domain) {
		return strpos($domain, '.') > -1;
	}

	public static function getTld($domain) {
		$split = explode('.', $domain, 2);

		return end($split);
	}

	/**
	 * Get domain along with basic link related stats
	 */
	public function withLinkData($id) {
		return Cache::remember("domain-{$id}", 1, function() use ($id){
			$domain = $this->findOrFail($id);
			$links = \App\Link::with('clicks')->whereDomainId($id)->get();

			$domain->setAttribute('links_count', $links->count());
			$domain->setAttribute('clicks_count', $links->reduce(function($carry, $link){
				return $carry + $link->clicks->count();
			}));
			$domain->setAttribute('average_click_count', $links->count() > 0 ?
				round($domain->clicks_count / $links->count())
				: 0);

			return $domain;
		});
	}
}
