<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Click extends Model
{
    protected $table = 'clicks';

    protected $guarded = [];

    public function scopeNoBots($query) {
    	return $query->where('browser', '!=', 'Bot')
    				 ->where('browser_version', '!=', '0');
    }
}
