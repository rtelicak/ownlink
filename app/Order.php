<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
	protected $table = 'orders';

	protected $fillable = ['domain_id', 'data'];

	protected $casts = [
		'data' => 'array',
	];

	public function setDataAttribute($data) {
		$this->attributes['data'] = json_encode($data);
	}
}
