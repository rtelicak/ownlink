<?php

namespace App\Jobs;

use \App\Ownlink\Detectors\GeoDetector;
use \App\Ownlink\Detectors\DeviceDetector;
use \App\Ownlink\Detectors\RefererDetector;

use App\Jobs\Job;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class AnalyzeLinkClick extends Job implements SelfHandling, ShouldQueue
{
	use InteractsWithQueue, SerializesModels;

	protected $server;

	protected $linkId;

	/**
	 * Create a new job instance.
	 *
	 * @return void
	 */
	public function __construct(array $server, $linkId) {
		$this->server = $server;
		$this->linkId = $linkId;
	}

	/**
	 * Execute the job.
	 *
	 * @return void
	 */
	public function handle() {
		if (is_null($this->linkId)) return;

		$this->refeshDatabaseConnection();

		\App\Click::create(array_merge(
			(new GeoDetector($this->server))->getData(),
			(new DeviceDetector($this->server))->getData(),
			(new RefererDetector($this->server))->getData(),
			['link_id' => $this->linkId]
		));
	}

	// https://laracasts.com/discuss/channels/requests/when-to-use-daemon-queue-workers?page=1
	// https://laravel.com/docs/5.1/queues#daemon-queue-listener
	// database connection may disconnect when being used by a long-running daemon
	private function refeshDatabaseConnection(){
		try {
			\DB::connection()->getDatabaseName();
		}
		catch (PDOException $e) {
			\DB::reconnect();
		}
	}
}
