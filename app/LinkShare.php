<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LinkShare extends Model {

    protected $table = 'link_share';

    protected $fillable = ['type'];
}
