var $ = require('jquery');

$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
	}
});

var LinkDAO = {
	/**
	 * Get link
	 */
	get(linkId, successCallback, failCallback){
		$.get('/admin/svc/links/' + linkId)
		.done(newLink => {
			successCallback(newLink);
		})
		.fail(err => {
			failCallback(err);
		});
	},

	syncTags(data, successCallback, failCallback){
		$.post('/admin/svc/syncTags/', data)
		.done(newLink => {
			successCallback(newLink);
		})
		.fail(err => {
			failCallback(err);
		});
	},

	togglePinned(id, successCallback, failCallback) {
		$.get(`/admin/svc/links/${id}/togglePin`)
		.done(successCallback)
		.fail(failCallback);
	},

	/**
	 * Load paginated links
	 */
	loadMoreLinks(domainId, page, successCallback, failCallback){
		$.get('/admin/svc/domains/' + domainId + '/links/paginated/?page=' + page)
		.done(successCallback)
		.fail(failCallback);
	},

	/**
	 * Create new Link
	 */
	create(link, successCallback, failCallback){
		$.post('/admin/svc/links', link)
		.done(newLink => {
			successCallback(newLink);
		})
		.fail(err => {
			failCallback(err);
		});
	},

	/**
	 * Delete link
	 */
	delete(linkId, successCallback, failCallback) {
		$.ajax({
			url: '/admin/svc/links/' + linkId,
			type: 'DELETE',
		})
		.done(response => {
			successCallback(response);
		})
		.fail(err => {
			failCallback(err);
		});
	},
};

module.exports = LinkDAO;