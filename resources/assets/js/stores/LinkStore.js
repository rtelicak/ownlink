var BaseDispatcher = require('../dispatcher/BaseDispatcher');
var LinkConstants = require('../constants/LinkConstants');
var LinkActions = require('../actions/LinkActions');
var EventEmitter = require('events').EventEmitter;
var LinkDAO = require('../api/LinkDAO');
var assign = require('object-assign');
var _ = require('underscore');
var history = require('../utilities/history');

var CHANGE_EVENT = 'change';

var _domainData = window.domainData;
var _selectedLinkId = window.domainData.links.length ? window.domainData.links[0].id : 0;
var _currentPage = 1;
var _snackbarMessage = {
	type: '',
	message: ''
};

history.listen(arg => {
	var match = arg.pathname.match('/links/([0-9]+)');

	if(match && match.length) _selectedLinkId = match[1];
});

function appendLoadedLinks(links) {
	_domainData.links.data = _domainData.links.data.concat(links);
}

function refreshSelectedLink() {
	LinkDAO.get(LinkStore.getSelectedLinkId(),
		link => {
			_.extend(LinkStore.getSelectedLink(), link);
			setSnackbarMessage('Link successfuly shared', 'success');
			LinkStore.emitChange();
		},
		err => {
			console.log(err);
	});
}

function updateLink(updatedLink, message){
	_domainData.links.data = _.map(_domainData.links.data, link => {
		if (updatedLink.id == link.id) {
			link.is_pinned = updatedLink.is_pinned
		}

		return link;
	});
}

function sortLinks(){
	_domainData.links.data.sort((a,b) => {
		if (a.is_pinned && !b.is_pinned) return -1;
		if (b.is_pinned && !a.is_pinned) return 1;

		if ((new Date(a.created_at)).getTime() < (new Date(b.created_at)).getTime()) return 1;
		if ((new Date(a.created_at)).getTime() > (new Date(b.created_at)).getTime()) return -1;
	});
}

function loadMoreLinks(){
	LinkDAO.loadMoreLinks(_domainData.id, ++_currentPage,
		response => {
			LinkActions.appendLoadedLinks(response);
		},
		err => {
			setSnackbarMessage('error', err);
	});
}

function deleteSelectedLink(){
	LinkDAO.delete(_selectedLinkId,
		response => {
			LinkActions.deleteLinkCompleted(response);
		},
		err => {
			LinkActions.deleteLinkFailed(err);
	});
}

function syncTags(linkId, tags){
	LinkDAO.syncTags({linkId, tags},
		syncedLink => {
			_.extend(LinkStore.getSelectedLink(), syncedLink);
			setSnackbarMessage('Link tags updated', 'success');
			LinkStore.emitChange();
		},
		err => {
			console.log(err);
	});
}

function handleNewLink(link, message){
	link.clicks = [];
	link.tags = [];
	link.click_rate = 0;
	_domainData.links.data.unshift(link);
	_selectedLinkId = link.id;
	setSnackbarMessage(message, 'success');
}

function handleDeletedLink(deletedLink, message){
	_domainData.links.data = _domainData.links.data.filter(function(link){
		return link.id != deletedLink.id;
	});
	_selectedLinkId = _domainData.links.data.length ? _domainData.links.data[0].id : '';

	history.replaceState(null, `/links/${_selectedLinkId}`);

	setSnackbarMessage(message, 'success');
}

function setSnackbarMessage(message = '', type = ''){
	_snackbarMessage = {message, type};

	setTimeout(() => {
		_snackbarMessage = {message : '', type: ''};
	}, 1500);
}

var LinkStore = assign({}, EventEmitter.prototype, {

	getDomainData: function() {
		return _domainData;
	},

	getSelectedLink: function(){
		return _.find(domainData.links.data, link => {
			return link.id == _selectedLinkId;
		});
	},

	getSelectedLinkId(){
		return _selectedLinkId;
	},

	getSnackbarMessage(){
		return _snackbarMessage;
	},

	emitChange: function() {
		this.emit(CHANGE_EVENT);
	},

	addChangeListener: function(callback) {
		this.on(CHANGE_EVENT, callback);
	},

	removeChangeListener: function(callback) {
		this.removeListener(CHANGE_EVENT, callback);
	}
});

// Register callback to handle all updates
BaseDispatcher.register(function(action) {

	switch(action.actionType) {
		/**
		 * Link was shared on social media
		 */
		case LinkConstants.SYNC_TAGS:
			syncTags(action.linkId, action.tags);
			break;

		/**
		 * Link was shared on social media
		 */
		case LinkConstants.LINK_WAS_SHARED:
			refreshSelectedLink();
			break;

		/**
		 * Loading more links
		 */
		case LinkConstants.LOAD_MORE_LINKS:
			loadMoreLinks();
			break;


		/**
		 * Link's `pin` flag has been toggled
		 */
		case LinkConstants.LINK_PING_TOGGLED:
			updateLink(action.link, action.message);
			sortLinks();
			LinkStore.emitChange();
			break;

		case LinkConstants.LOAD_MORE_LINKS_COMPLETED:
			appendLoadedLinks(action.links);
			LinkStore.emitChange();
			break;

		/**
		 * CREATING LINK
		 */
		case LinkConstants.LINK_CREATE_COMPLETED:
			handleNewLink(action.link, action.message);
			LinkStore.emitChange();
			break;

		/**
		 * DELETING LINK
		 */
		case LinkConstants.LINK_DELETE:
			deleteSelectedLink();
			break;

		case LinkConstants.LINK_DELETE_COMPLETED:
			handleDeletedLink(action.link, action.message);
			LinkStore.emitChange();
			break;

		case LinkConstants.LINK_DELETE_FAILED:
			setSnackbarMessage(action.err.responseJSON.message, 'error');
			LinkStore.emitChange();
			break;

		default:
		// no op
	}
});

module.exports = LinkStore;
