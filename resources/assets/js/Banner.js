import React from 'react';
import jQuery from 'jquery';
import Button from './components/_common/Button';
import mui from 'material-ui';
import { ThemeManager, TextField, Card, CardTitle, CardText, Snackbar, RefreshIndicator } from 'material-ui';
import history from './utilities/history';

var Banner = React.createClass({

	getDefaultProps: function() {
		return {
			domainData: window._domainData
		};
	},

	childContextTypes: {
		muiTheme: React.PropTypes.object
	},

	getChildContext: function() {
		return {
			muiTheme: new mui.Styles.ThemeManager().getCurrentTheme()
		};
	},

	componentDidMount: function() {
		setTimeout(() => {
			this.refs.original.focus();
		}, 1000);

		jQuery.ajaxSetup({
			headers: {
				'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content'),
			}
		});
	},

	getInitialState: function() {
		return {
			original: 'about:blank',
			custom: 'my-link',
			button_text: 'click this button',
			button_target: 'http://some-my-page.com',
			banner_message: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit.',
			is_redirect: 0,
			domain_id: this.props.domainData.id,
			loadingState: '',
			formErrors: []
		};
	},

	_handleChage(changedRef){
		return () => {
			this.setState({
				[changedRef]: this.refs[changedRef].getValue()
			});
		};
	},

	handleSubmit(e) {
		e.preventDefault();

		this.setState({
			loadingState: 'loading'
		});

		jQuery.post('/admin/svc/links', this.state)
		.done(this._handleDone)
		.fail(this._handleFail);
	},

	_handleDone(response){
		this.setState({
			loadingState: 'loaded',
			message: response.message
		});

		setTimeout(() => {
			window.location = '/admin';
		}, 1000);
	},

	_handleFail(err){
		this.setState({
			formErrors: err.responseJSON,
			loadingState: ''
		});
	},

	_showFormError(ref) {
		return this.state.formErrors[ref] ? this.state.formErrors[ref][0] : '';
	},

	getCustomRefLabelText(){
		if (!this.state.custom.length) return 'Your Custom Link';

		return 'Your Custom Link (' + this.props.domainData.name + '/' + this.state.custom + ')';
	},

	render: function() {
		return (
			<div>
				{this._renderSnackBar()}
				<div className="link-banner__overlay">
					<div className="container">
						<form onSubmit={this.handleSubmit} className="form-horizontal row">
							<div className="col-xs-6 col-xs-offset-3 link-banner__form-wrapper">
								<Card>
									<CardTitle title="Create Banner Link"/>
									<CardText className="foo">

									<TextField
										ref='original'
										fullWidth={true}
										errorText={this._showFormError('original')}
										defaultValue={this.state.original}
										floatingLabelText="Enter a URL"
										onChange={this._handleChage('original')} />

									<TextField
										ref='custom'
										fullWidth={true}
										errorText={this._showFormError('custom')}
										defaultValue={this.state.custom}
										floatingLabelText={this.getCustomRefLabelText()}
										onChange={this._handleChage('custom')} />

									<TextField
										ref='banner_message'
										fullWidth={true}
										errorText={this._showFormError('banner_message')}
										defaultValue={this.state.banner_message}
										floatingLabelText="Message"
										onChange={this._handleChage('banner_message')} />

									<TextField
										ref='button_text'
										fullWidth={true}
										errorText={this._showFormError('button_text')}
										defaultValue={this.state.button_text}
										floatingLabelText="Button Text"
										onChange={this._handleChage('button_text')} />

									<TextField
										ref='button_target'
										fullWidth={true}
										errorText={this._showFormError('button_target')}
										defaultValue={this.state.button_target}
										floatingLabelText="Button Url (Where should the button link to?)"
										onChange={this._handleChage('button_target')} />

									<div style={{
											position:'relative',
											minHeight: '30px',
											minWidth: '30px',
											textAlign: 'center',
											display: 'inline-block',
											margin: '0 auto',
											top: '10px'
										}}>
										{this._renderButtons()}
									</div>
									</CardText>
								</Card>
							</div>
						</form>
					</div>
				</div>

				<iframe id="ContentFrame" src={this.state.original}></iframe>

				<div id="BannerFrame">
					<div className="link-banner">
						<div className="link-banner__brand">
							<a href={this.props.domainData.config.redirectUrl}>
								<img src={this.props.domainData.config.brandImageUrl} width='50px' height='50px'/>
							</a>
							<a className="link-banner__brand__name" href={this.props.domainData.config.redirectUrl}>
								{this.props.domainData.config.brandName}
							</a>
						</div>

						<div className="link-banner__message_wrapper">
							<span className="link-banner__message">{this.state.banner_message}</span>
							<a id="collectConversion" className="link-banner__cta-button" target="_blank" title={this.state.button_target}>
								{this.state.button_text}
							</a>
						</div>

						<a className="link-banner__close" href={this.state.original}>&#215;</a>
					</div>
				</div>
			</div>
		);
	},

	_renderButtons() {
		if (this.state.loadingState == 'loaded') return null;

		if (this.state.loadingState == 'loading') {
			return <RefreshIndicator
				className="_refreshIndicator"
				size={30}
				left={0}
				top={0}
				status="loading" />;
		}

		return (
			<div>
				<Button
					text="Cancel"
					className='btn-default'
					isLink={true}
					onClick={this._handleCancel}
					style={{float: 'left'}} />

				<Button
					type='submit'
					className='btn-primary btn-raised'
					text="Create Banner Link" />
			</div>);
	},

	_renderSnackBar(){
		return this.state.loadingState == 'loaded' ?
			<Snackbar
				message={this.state.message}
				autoHideDuration={2000}
				openOnMount={true}
				style={{
					backgroundColor: '#43a047',
					bottom: '80'
				}} />
			: null;
	},

	_handleCancel(){
		history.goBack();
	}
});

React.render(<Banner />, document.getElementById('screen'));