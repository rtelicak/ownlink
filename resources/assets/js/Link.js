var React = require('react');
var LinkStore = require('./stores/LinkStore');
var LinkActions = require('./actions/LinkActions');
var LinkSection = require('./components/LinkSection');
var LinkForm = require('./components/LinkForm');
import connectToStores from './utilities/connect-to-stores';

var Dashboard = React.createClass({

	render: function() {
		return (
			<div>
				<div className="row screen-header">
					<div className="col-xs-6">
						<div className="screen-title">
							Your Links
							<span className="screen-title__small">&nbsp; [{ this.props.domainData.name }]</span>
						</div>
					</div>
					<div className="col-xs-6">
						<LinkForm />
					</div>
				</div>
				<div className="row">
					{this._renderLinks()}
					{this.props.children}
				</div>
			</div>
		);
	},

	_renderLinks(){
		var nodes;

		if (!this.props.domainData.links) {
			nodes = <div className="no-data-message col-xs-12">You have no links yet :(<br/>Start with creating one! &#8599;</div>
		} else {
			nodes = <LinkSection selectedLinkId={this.props.params.linkId}/>;
		}

		return nodes;
	},

	componentDidMount(){
		if (!this.props.params.linkId) {
			this.props.history.pushState(null, `/links/${this.props.domainData.links.data[0].id}`);
		}
	},
});

module.exports = connectToStores(Dashboard, [LinkStore], props => ({
	domainData: LinkStore.getDomainData()
}));