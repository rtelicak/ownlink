var BaseDispatcher = require('../dispatcher/BaseDispatcher');
var LinkConstants = require('../constants/LinkConstants');
import LinkStore from '../stores/LinkStore';
import LinkDAO from '../api/LinkDAO';

var LinkActions = {

	linkShared(index){
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LINK_WAS_SHARED,
			index: index
		});
	},

	toggleLinkPinned(id){
		LinkDAO.togglePinned(
			id,
			(response) => BaseDispatcher.dispatch({
				actionType: LinkConstants.LINK_PING_TOGGLED,
				link: response.link,
				message: response.message,
			}),
			() => console.log('zle je')
		);
	},

	syncTags(linkId, tags) {
		BaseDispatcher.dispatch({
			actionType: LinkConstants.SYNC_TAGS,
			linkId: linkId,
			tags: tags
		});
	},

	loadMoreLinks(){
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LOAD_MORE_LINKS
		});
	},

	appendLoadedLinks(data){
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LOAD_MORE_LINKS_COMPLETED,
			links: data.data
		});
	},

	createLinkCompleted: function(response) {
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LINK_CREATE_COMPLETED,
			link: response.link,
			message: response.message
		});
	},

	deleteSelectedLink: function() {
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LINK_DELETE,
		});
	},

	deleteLinkCompleted: function(response) {
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LINK_DELETE_COMPLETED,
			message: response.message,
			link: response.link
		});
	},

	deleteLinkFailed(err) {
		BaseDispatcher.dispatch({
			actionType: LinkConstants.LINK_DELETE_FAILED,
			err: err
		});
	},
};

module.exports = LinkActions;
