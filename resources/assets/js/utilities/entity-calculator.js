module.exports = function(clicksArray, entityName){
	var values = {};

	if (!clicksArray.length) return;

	clicksArray.forEach(function(click){
		if (click[entityName] === "") {
			click[entityName] = 'Unknown';
		}

		if (click[entityName] && values[click[entityName]] === undefined){
			values[click[entityName]] = 1;
		} else if(click[entityName] !== '') {
			values[click[entityName]]++;
		}
	});

	return values;
};