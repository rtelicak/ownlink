'use strict';
var colors = require('./colors.json');

module.exports = function(color){
	if (color in colors) {
		return colors[color];
	}

	return '#eee';
};