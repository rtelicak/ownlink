var keyMirror = require('keymirror');

module.exports = keyMirror({
	SYNC_TAGS: null,
	LINK_DELETE: null,
	LINK_WAS_SHARED: null,
	LINK_DELETE_FAILED: null,
	LOAD_MORE_LINKS: null,
	LOAD_MORE_LINKS_COMPLETED: null,
	LOAD_MORE_LINKS_FAILED: null,
	LINK_CREATE_COMPLETED: null,
	LINK_DELETE_COMPLETED: null,
	SHOW_LINK_REDIRECT_FORM: null,

	LINK_PING_TOGGLED: null
});
