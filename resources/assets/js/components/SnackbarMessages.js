import React from 'react';
import { Snackbar } from 'material-ui';
import LinkStore from '../stores/LinkStore';
import connectToStores from '../utilities/connect-to-stores';

var snackbarMessages =  React.createClass({
	componentDidUpdate(nextProps) {
		this.props.snackbarMessage.message && this.refs.snackBar.show();
	},

	render: function() {
		if (! this.props.snackbarMessage.message) return null;

		var bgColor = this.props.snackbarMessage.type == 'error' ? '#f44336' : '#43a047';

		return (
			<Snackbar
				message={this.props.snackbarMessage.message}
				autoHideDuration={2000}
				openOnMount={true}
				ref="snackBar"
				style={{
					backgroundColor: bgColor,
				}} />
		);
	},
});

module.exports = connectToStores(snackbarMessages, [LinkStore], props => ({
	snackbarMessage: LinkStore.getSnackbarMessage()
}));