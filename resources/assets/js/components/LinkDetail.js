import React from 'react';
import moment from 'moment';
import LinkStore from '../stores/LinkStore';
import connectToStores from '../utilities/connect-to-stores';

import { LinkGeneralInfoContainer, LinkBrowserInfo, LinkRefererInfo,
		 LinkDeviceInfo, LinkTimeChart, UnfollowLink } from './Link/index';

var LinkDetail = React.createClass({

	shouldComponentUpdate(nextProps, nextState) {
		return this.props.selectedLink.id != nextProps.selectedLink.id;
	},

	render: function() {
		if (!this.props.selectedLink) {
			return null;
		}

		return (
			<div className="col-xs-8">
				<div className="action-buttons">
					<UnfollowLink domainName={this.props.domainData.name} link={this.props.selectedLink}/>
				</div>
				<LinkGeneralInfoContainer link={this.props.selectedLink} domainData={this.props.domainData}/>
				<div className="row">
					<div className="col-xs-12">
						<LinkTimeChart chartData={this.props.selectedLink.chart_data}/>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-6">
						<LinkRefererInfo link={this.props.selectedLink} domainData={this.props.domainData}/>
					</div>
					<div className="col-xs-6">
						<LinkBrowserInfo link={this.props.selectedLink} domainData={this.props.domainData}/>
					</div>
				</div>
				<div className="row">
					<div className="col-xs-6">
						<LinkDeviceInfo link={this.props.selectedLink} domainData={this.props.domainData}/>
					</div>
					<div className="col-xs-6">

					</div>
				</div>
			</div>
		);
	},
});

module.exports = connectToStores(LinkDetail, [LinkStore], props => ({
	selectedLink: LinkStore.getSelectedLink(),
	domainData: LinkStore.getDomainData()
}));
