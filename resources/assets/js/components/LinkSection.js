import React from 'react';
import Button from './_common/Button';
import LinkListItem from './Link/ListItem';
import LinkStore from '../stores/LinkStore';
import LinkActions from '../actions/LinkActions';
import connectToStores from '../utilities/connect-to-stores';
import { Link } from 'react-router';


var LinkSection = React.createClass({

	handleLoadMoreClick(){
		LinkActions.loadMoreLinks();
	},

	_handleDoubleClick(id){
		LinkActions.toggleLinkPinned(id);
	},

	render: function() {
		// domain has no links created yet
		if (this.props.domainData.links.data.length === 0) {
			return (
				<div className="col-xs-12">
					<h1><small>You don't have any Links yet :(</small></h1>
				</div>
			);
		}

		var links = this.props.domainData.links.data.map((link, index) => {
			return (
			<Link onDoubleClick={this._handleDoubleClick.bind(this, link.id)}
				  key={link.id}
				  to={`/links/${link.id}`} >
					<LinkListItem
						domainData={this.props.domainData}
						link={link}
						selectedLinkId={this.props.selectedLinkId} />
			</Link>);
		});

		return (
			<div className="link-section col-xs-4">
				<div className="row">
					<div className="link-section__stats col-xs-4">
						<div className="link-section__stats__value">{this.props.domainData.links_count}</div>
						<div className="link-section__stats__label small">links</div>
					</div>
					<div className="link-section__stats col-xs-4">
						<div className="link-section__stats__value">{this.props.domainData.clicks_count}</div>
						<div className="link-section__stats__label small">clicks</div>
					</div>
					<div className="link-section__stats col-xs-4">
						<div className="link-section__stats__value">{this.props.domainData.average_click_count}</div>
						<div className="link-section__stats__label small">average</div>
					</div>
				</div>
				<ul className="link-list list-unstyled">{links}</ul>
				{this.renderPaginateButton()}
			</div>
		);
	},

	renderPaginateButton(){
		if(this.props.domainData.links_count > this.props.domainData.links.data.length) {
			return (
			<div className="tac">
				<Button
					className="btn-default"
					onClick={this.handleLoadMoreClick}
					text={'Load more'}
				/>
			</div>);
		}

		return null;
	},
});

module.exports = connectToStores(LinkSection, [LinkStore], props => ({
	domainData: LinkStore.getDomainData()
}));