var React = require('react');
var LineChart = require("react-chartjs").Line;
var DataPanel = require('./DataPanel');

var TimeChart = React.createClass({

	// propTypes: {
	// 	chartData: React.PropTypes.object.isRequired
	// },

	getChartData(){
		var data = [];
		for(var date in this.props.chartData) {
			data.push(this.props.chartData[date]);
		}

		return {
			labels: Object.keys(this.props.chartData),
			datasets: [{
				fillColor: "#E1BEE7",
				strokeColor: "#AB47BC",
				pointColor: "#9C27B0",
				pointStrokeColor: "#fff",
				pointHighlightFill: "#9C27B0",
				pointHighlightStroke: "#9C27B0",
				data: data
			}]
		};
	},

	render: function() {
		if (!this.props.chartData || !Object.keys(this.props.chartData).length) {
			return null;
		}

		var chartOptions = {
			scaleGridLineColor: '#F0F0F0',
			scaleShowVerticalLines: false,
			scaleFontFamily: "'Roboto', sans-serif",
			scaleLineColor: 'F0F0F0',
			tooltipTemplate: "<%= value %>"
		};

		var body = <LineChart
				data={this.getChartData()}
				options={chartOptions}
				redraw
				width="710"
				height="200"/>

		return <DataPanel body={body} title={'Clicks in time'} />;
	}

});

module.exports = TimeChart;