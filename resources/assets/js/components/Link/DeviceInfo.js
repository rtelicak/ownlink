var React = require('react');
var DoughnutChart = require("react-chartjs").Doughnut;
var ChartLegend = require("../ChartLegend");
var Color = require("color");
var brandColors = require('../../utilities/brand-colors');
var DataPanel = require('./DataPanel');

var DeviceInfo = React.createClass({

    propTypes: {
        link: React.PropTypes.object.isRequired,
        domainData: React.PropTypes.object.isRequired
    },

    render: function() {
        var devices = this._calculateChartData();

        if (!devices) return null;

        var chartData = [];

        for(var device in devices) {
            chartData.push({
                value: devices[device],
                label: device,
                color: brandColors(device.toLowerCase()),
                highlight: Color(brandColors(device.toLowerCase())).darken(0.2).hexString(),
            });
        }

        var charOptions = {percentageInnerCutout: 60};

        var body = <div>
            <ChartLegend datasets={chartData} />
            <DoughnutChart
                data={chartData}
                options={charOptions}
                redraw
                width="200"
                height="200"/>
            </div>;

        return <DataPanel body={body} title={'Devices'} />;
    },

    _calculateChartData(){
        var data = {
            mobile: 0,
            tablet: 0,
            desktop: 0,
        };

        if(!this.props.link.clicks || !this.props.link.clicks.length) return;

        this.props.link.clicks.forEach(function(click){
            if (click.is_mobile) data['mobile']++;
            if (click.is_tablet) data['tablet']++;
            if (click.is_desktop) data['desktop']++;
        });

        return data;
    }

});

module.exports = DeviceInfo;