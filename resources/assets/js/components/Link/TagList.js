var _ = require('underscore');
var $ = require('jquery');
var React = require('react/addons');
var classNames = require('classnames');
var TagsInput = require('react-tagsinput');
var LinkActions = require('../../actions/LinkActions');

var TagList = React.createClass({

	mixins: [React.addons.LinkedStateMixin],

	componentWillReceiveProps(nextProps){
		this.setState({
			tags: nextProps.link.tags.length ? nextProps.link.tags : this.defaultTag(),
			completions: []
		});
	},

	getInitialState: function () {
		return {
			tags: this.tags(),
			completions: []
		};
	},

	getDefaultProps(){
		return {
			hasTagsChanged: false
		};
	},

	defaultTag(){
		return [{
			id: 0,
			name: 'You haven\'t share this link yet',
			type: 'warning'
		}];
	},

	tags(){
		if(this.props.link.tags.length) return this.props.link.tags;

		return this.defaultTag();
	},

	allCompletions(){
		return [
			{
				id: 1,
				name: 'shared on facebook',
				type: 'facebook'
			},
			{
				id: 2,
				name: 'shared on twitter',
				type: 'twitter'
			},
			{
				id: 3,
				name: 'mail',
				type: 'mail'
			}
		];
	},

	complete: function (term) {
		term = term.toLowerCase().trim();

		if (term === '') {
			return this.setState({
				completions: []
			});
		}

		this.setState({
			completions: _.filter(this.allCompletions(), completion => {
					return completion.name.toLowerCase().indexOf(term) > -1 // does completion contain such string - fuzzy search
						&& _.pluck(this.state.tags, 'name').indexOf(completion.name) == -1; // link doesn't have such tag yet
			})
		});
	},

	_handleKeyPress(key) {
		if (key.keyCode != 13) return;

		// enter pressed
		if (this.state.completions.length == 1) {
			this.addTag(this.state.completions[0]);
		}
	},

	// just overriding default - trim function
	transformTag(tag){
		return tag;
	},

	renderTag(key, tag, removeHandler) {
		// dont show delete button on default tags
		var removeButton = tag.id != 0 ? <a className="link-label__remove" onClick={removeHandler}></a> : null;

		var cssClasses = classNames(
			'link-label',
			'link-label--' + tag.type
		);

		return (
			<span className={cssClasses}>
				<span>{tag.name}</span>
				{removeButton}
			</span>);
	},

	syncTags(newTags){
		if (!this.props.hasTagsChanged) return;

		// persist new tags to link
		var tagIds = _.without(_.pluck(newTags, 'id'), 0);
		LinkActions.syncTags(this.props.link.id, tagIds);

		// don't show default tag if there is another one
		var tags = _.filter(newTags, tag => {
			return tag.id != 0;
		});

		this.setState({
			tags: tags.length ? tags : this.defaultTag(),
			completions: []
		});

		this.props.hasTagsChanged = false;
	},

	addTag(tag){
		this.refs.tags.addTag(tag);
	},

	beforeTagAdd(tag){
		if (_.isObject(tag) && _.pluck(this.allCompletions(), 'name').indexOf(tag.name) > -1) {
			return this.props.hasTagsChanged = true;
		}

		return false;
	},

	beforeTagRemove(tag){
		if (tag.id != 0) {
			return this.props.hasTagsChanged = true;
		}

		return false;
	},

	render: function() {
		var completionNodes = this.state.completions.map(comp => {
			var cssClasses = classNames('link-label', 'link-label--' + comp.type)
			return <span className={cssClasses} onClick={this.addTag.bind(this, comp)}>{comp.name}</span>;
		});

		return (
			<div style={{width: 'calc(100% - 95px)'}} className="link-tags">
				<TagsInput
					ref='tags'
					classNamespace={'link-tags'}
					value={this.state.tags}
					renderTag={this.renderTag}
					onChange={this.syncTags}
					transform={this.transformTag}
					onChangeInput={this.complete}
					onKeyUp={this._handleKeyPress}
					beforeTagAdd={this.beforeTagAdd}
					beforeTagRemove={this.beforeTagRemove}
					addOnBlur={false}
				/>
				<div style={this._calculateOffset()} className="tag-completions">{completionNodes}</div>
			</div>
		);
	},

	_calculateOffset(){
		// TODO: isMounted ?
		if (! $('.link-tags-tagsinput').length) return {};

		var offset = Math.abs($('.link-tags-tagsinput').offset().left - $('.link-tags-tagsinput-input').offset().left);

		return {left: offset};
	},
});

module.exports = TagList;