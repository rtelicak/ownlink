var React = require('react');
var moment = require('moment');
var classNames = require('classnames');

var ListItem = React.createClass({

	render: function() {
		var cssClasses = classNames(
			'link-list-item',
			{'link-list-item--selected': this.props.link.id == this.props.selectedLinkId}
		);

		return (
			<li className={cssClasses} key={this.props.link.id}>
				<span className="link-list-item__date">{moment(this.props.link.created_at).format('D. MMM')}</span>
				<h4 className="link-list-item__title">{this.props.link.title}</h4>
				<div className="link-list-item__own_link">{this.props.domainData.name}/{this.props.link.custom}</div>
				{this._getBrandedLabel()}
				{this._prettifyClickRate()}
			</li>
		);
	},

	_getBrandedLabel: function() {
		var label = this.props.link.is_redirect ?
			<div className="link-label link-label--redirect">REDIRECT LINK</div> :
			<div className="link-label link-label--cta">CTA LINK</div>;

		var pin = this.props.link.is_pinned ?
			<span className="link-label link-label--pin">&nbsp;pinned</span> :
			null;

		return [label, pin];
	},

	_prettifyClickRate(){
		// TODO: more complex colors based on result
		if (this.props.link.click_rate > 0) return <div className="link-list-item__click-rate link-list-item__click-rate--up">&#8593;{this.props.link.click_rate}%</div>

		return <div className="link-list-item__click-rate link-list-item__click-rate--down">&#8595;{this.props.link.click_rate}%</div>;
	}
});

module.exports = ListItem;