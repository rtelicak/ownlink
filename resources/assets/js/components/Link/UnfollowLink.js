import React from 'react';
import {Dialog, Snackbar} from 'material-ui';
import LinkActions from '../../actions/LinkActions';
import Button from '../_common/Button';


var UnfollowLink = React.createClass({

	propTypes: {
		link: React.PropTypes.object.isRequired
	},

	_showDialog(){
		this.refs.dialog.show();
	},

	unfollowLink(){
		LinkActions.deleteSelectedLink();
		this.handleDialogCancel();
		console.log('deleting link');
	},

	handleDialogCancel(){
		this.refs.dialog.dismiss();
	},

	render: function() {
		var customActions = [
			<Button onClick={this.handleDialogCancel} className="btn-default" text={'Cancel'}/>,
			<Button onClick={this.unfollowLink} type="submit" className="btn-danger" text={'Unfollow'}/>
		];

		return (
			<div>
				<Button
					onClick={this._showDialog}
					className="btn-danger"
					text={'Unfollow'}
				/>

				<Dialog
					ref="dialog"
					title="Not interested in this link anymore?"
					onShow={this.onDialogShow}
					onDismiss={this.onDialogDismiss}
					autoScrollBodyContent={false}
					actions={customActions}>
				<div className="link-dialog">
					Are you sure you want to unfollow <strong>{this.props.domainName}/{this.props.link.custom}</strong>? Link will still work, but you will not see it in your dashboard.<br/>
					Even if you unfolllow this one, you will not be able to create new <em>{this.props.link.custom}</em> link.
				</div>
				</Dialog>
			</div>
		);
	}

});

module.exports = UnfollowLink;