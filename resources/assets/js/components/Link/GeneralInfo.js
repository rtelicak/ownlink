import React from 'react';
import moment from 'moment';

export default class GeneralInfo extends React.Component {
	render() {
		if (this.props.link.is_redirect) {
			return this._renderRedirectLink();
		}

		return this._renderCTALink();
	}

	_renderCTALink(){
		let duration = moment.duration(this.props.link.stats.time_spent_count, 'seconds');
		let avgConversion = this.props.link.stats.conversion_count > 0 ?
			(this.props.link.stats.conversion_count / this.props.link.clicks.length * 100).toFixed(2)
			: 0;

		return (
			<div className="row">
				<div className="col-xs-3 link-detail__stats">
					<h2>{this.props.link.clicks.length}</h2>
					<div className="small">clicks</div>
				</div>
				<div className="col-xs-3 link-detail__stats">
					<h2>{this.props.link.stats.conversion_count}</h2>
					<div className="small">conversions</div>
				</div>
				<div className="col-xs-3 link-detail__stats">
					<h2>{avgConversion}%</h2>
					<div className="small">avg conversion</div>
				</div>
				<div className="col-xs-3 link-detail__stats">
					<h2>{duration.get('minutes')}:{duration.get('seconds')}</h2>
					<div className="small">time on site</div>
				</div>
			</div>
		);
	}

	_renderRedirectLink(){
		let weekAgo = moment().day(-7);
		let today = moment();

		let thisWeek = this.props.link.clicks.filter(click =>{
			return moment(click.created_at).isBetween(weekAgo, today);
		});

		return (
			<div className="row">
				<div className="col-xs-4 link-detail__stats">
					<h2>{this.props.link.clicks.length}</h2>
					<div className="small">clicks</div>
				</div>
				<div className="col-xs-4 link-detail__stats">
					<h2>{thisWeek.length}</h2>
					<div className="small">this week</div>
				</div>
				<div className="col-xs-4 link-detail__stats">
					<h2>{this.props.link.click_rate}%</h2>
					<div className="small">click rate</div>
				</div>
			</div>
		);
	}
}
