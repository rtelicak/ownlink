var React = require('react');

var DataPanel = React.createClass({

	propTypes: {
		title: React.PropTypes.string.isRequired,
		body: React.PropTypes.node.isRequired
	},

	render: function() {
		return (
			<div className="fieldset">
				<div className="fieldset__title">{this.props.title}</div>
				<div className="fieldset__body">
					{this.props.body}
				</div>
			</div>
		);
	}

});

module.exports = DataPanel;