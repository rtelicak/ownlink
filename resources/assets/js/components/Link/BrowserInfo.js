var React = require('react');
var Color = require("color");
var DataPanel = require('./DataPanel');
var ChartLegend = require("../ChartLegend");
var DoughnutChart = require("react-chartjs").Doughnut;
var brandColors = require('../../utilities/brand-colors');
var entityCalculator = require('../../utilities/entity-calculator');


var BrowserInfo = React.createClass({

	propTypes: {
		link: React.PropTypes.object.isRequired,
		domainData: React.PropTypes.object.isRequired
	},

	render: function() {
		var browsers = entityCalculator(this.props.link.clicks, 'browser');

		if (!browsers) return null;

		var chartData = [];

		for(var browser in browsers) {
			chartData.push({
				value: browsers[browser],
				label: browser,
				color: brandColors(browser.toLowerCase()),
				highlight: Color(brandColors(browser.toLowerCase())).darken(0.1).hexString(),
			});
		}

		var charOptions = {percentageInnerCutout: 60};

		var body = <div>
			<ChartLegend datasets={chartData} />
			<DoughnutChart
				ref="referersChart"
				options={charOptions}
				data={chartData}
				redraw
				width="200"
				height="200"/>
			</div>;

		return <DataPanel body={body} title={'Browsers'} />;
	}

});

module.exports = BrowserInfo;