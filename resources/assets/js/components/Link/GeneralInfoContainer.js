import React from 'react';
import moment from 'moment';
import DataPanel from './DataPanel';
import TagList from './TagList';
import Button from '../_common/Button';
import GeneralInfo from './GeneralInfo';
import ReactZeroClipboard from 'react-zeroclipboard';
import FacebookSharer from '../Social/FacebookSharer';
import TwitterSharer from '../Social/TwitterSharer';

let MainInfo = React.createClass({

	propTypes: {
		link: React.PropTypes.object.isRequired,
		domainData: React.PropTypes.object.isRequired
	},

	getInitialState() {
		return {
			copyToClipboardText: 'Copy'
		};
	},

	componentWillReceiveProps(nextProps){
		this.setState(this.getInitialState());
	},

	_handleSetClipboard(){
		this.setState({
			copyToClipboardText: 'Copied :)'
		});
	},

	render() {
		this.props.link.fullUrl = 'http://' + this.props.domainData.name + '/' +this.props.link.custom;

		let body =
			<span>
				<div className="row">
					<div className="col-xs-9">
						{this._getLinkTypeTag()}
						<TagList link={this.props.link} />
					</div>
					<div className="col-xs-3">
						<div className="link-detail__date">Created:&nbsp;{moment(this.props.link.created_at).format('D. MMM, HH:mm')}</div>
					</div>
				</div>

				<div className="link-detail__original">
					<div className="link-detail__title">{this.props.link.title}</div>
					<div className="link-detail__target">[&nbsp;{this.props.link.original}&nbsp;]</div>
				</div>

				{this._renderNote()}

				<span className="link-detail__own">
					<a href={this.props.link.fullUrl}>{this.props.domainData.name}/{this.props.link.custom}</a>

					<ReactZeroClipboard getText={this.props.link.fullUrl}>
						<Button
							className="btn-default link-detail__copy_button"
							onClick={this._handleSetClipboard}
							text={this.state.copyToClipboardText}
							key={'fuckingButton'} />
					</ReactZeroClipboard>
				</span>

				<div className="row">
					<div className="col-xs-12">
						<div className="col-xs-6 tar">
							<FacebookSharer link={this.props.link}/>
						</div>
						<div className="col-xs-6">
							<TwitterSharer link={this.props.link}/>
						</div>
					</div>
				</div>

				<GeneralInfo link={this.props.link}/>
			</span>;


		return <DataPanel body={body} title={'General Info'} />;
	},

	_getLinkTypeTag() {
		if (this.props.link.is_redirect) return <div className="link-label link-label--redirect">REDIRECT LINK</div>;

		return <div className="link-label link-label--cta">CTA LINK</div>;
	},

	_renderNote(){
		if (this.props.link.note) return <div className="link-detail__note">{this.props.link.note}</div>;

		return null;
	},

});

module.exports = MainInfo;