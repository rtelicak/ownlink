var React = require('react');
var DoughnutChart = require("react-chartjs").Doughnut;
var ChartLegend = require("../ChartLegend");
var Color = require("color");
var brandColors = require('../../utilities/brand-colors');
var entityCalculator = require('../../utilities/entity-calculator');
var DataPanel = require('./DataPanel');

var RefererInfo = React.createClass({

	propTypes: {
		link: React.PropTypes.object.isRequired,
		domainData: React.PropTypes.object.isRequired
	},

	render: function() {
		var referers = entityCalculator(this.props.link.clicks, 'source');

		if (!referers) return null;

		var chartData = [];

		for(var referer in referers) {
			chartData.push({
				value: referers[referer],
				label: referer,
				color: brandColors(referer.toLowerCase()),
				highlight: Color(brandColors(referer.toLowerCase())).darken(0.2).hexString(),
			});
		}

		var chartOptions = {percentageInnerCutout: 60};

		var body = <div>
			<ChartLegend datasets={chartData} />
			<DoughnutChart
				data={chartData}
				options={chartOptions}
				redraw
				width="200"
				height="200"/>
			</div>;

		return <DataPanel body={body} title={'Referers'} />;
	}

});

module.exports = RefererInfo;