var React = require('react');
var $ = require('jquery');

var FindForm = React.createClass({

	fetchSuggestions: function(term) {
		return $.get('svc/suggest-domain', {term});
	},

	handleSubmit(e) {
		e.preventDefault();
		var inputValue = React.findDOMNode(this.refs.findInput).value;

		this.fetchSuggestions(inputValue)
		.done(suggestions => {
			if (suggestions) this.props.onSuggestionsChange(suggestions);
		})
		.fail(err => {
			this.showErrorMessage(err);
		});
	},

	showErrorMessage: function(err) {
		console.log(err);
	},

	render: function() {
		return (
			<form onSubmit={this.handleSubmit}>
				<div className="form-group">
					<input ref="findInput" type="text" className="form-control" placeholder="Company name e.g. Cool cupcake " />
				</div>
			</form>
		);
	}

});

module.exports = FindForm;