var React = require('react');
var $ = require('jquery');

var DomainSuggestion = React.createClass({

	getInitialState(){
		return {
			wasChecked: false,
			isAvailable: false
		};
	},

	componentWillReceiveProps(){
		if (this.state.wasChecked) {
			this.replaceState(this.getInitialState());
		}
	},

	checkAvailability(e){
		e.preventDefault();
		var name = this.props.data.domain + '.' + this.props.data.tld;

		$.get('svc/check-domain', {name})
		.done(result => {
			this.setState({
				wasChecked: true,
				isAvailable: result.isAvailable
			});
			// return this.props.onDomainCheck(result);
		})
		.fail(err => {
			console.log(err);
		});
		// return this.props.onAvailabilityCheck(this.props.data);
	},

	handleBuy(){
		$.get('svc/remember-domain', this.props.data)
		.done(result => {
			console.log(result);
			window.location = '/pay-for-it';
		})
		.fail(err => {

		});
	},

	getCheckedResult(){
		if (! this.state.wasChecked) return {__html: ''};

		var html = this.state.isAvailable ? '&nbsp;&#10003; Available' : '&nbsp;&#10007; Not Available';

		return {__html: html};
	},

	renderButton(){
		if (! this.state.wasChecked) 
			return <button onClick={this.checkAvailability} className="btn btn-primary">Check</button>;

		if (this.state.isAvailable) 
			return <button onClick={this.handleBuy} className="btn btn-primary">Buy</button>;

		return '';
	},

	render: function() {

		return (
			<div className="row">
				<div className="col-xs-4">
					<h4>{this.props.data.domain}.{this.props.data.tld}</h4></div>
				<div className="col-xs-4">
					<h4>{this.props.data.price}</h4>
				</div>
				<div className="col-xs-2">
					<h4>{this.props.data.available}</h4>
				</div>
				<div className="col-xs-2">
					{this.renderButton()}
					<span dangerouslySetInnerHTML={this.getCheckedResult()}></span>
				</div>
			</div>
		);
	}

});

module.exports = DomainSuggestion;