import React from 'react';
import jQuery from 'jquery'

var element, circle, d, x, y;

var Button = React.createClass({

	_doEffect(e){
		// stolen from http://codepen.io/madshaakansson/pen/ykode
		element = jQuery(this.getDOMNode());

		if(element.find(".circle").length == 0)
			element.prepend("<span class='circle'></span>");

		circle = element.find(".circle");
		circle.removeClass("animate");

		if(!circle.height() && !circle.width()) {
			d = Math.max(element.outerWidth(), element.outerHeight());
			circle.css({height: d, width: d});
		}

		x = e.pageX - element.offset().left - circle.width()/2;
		y = e.pageY - element.offset().top - circle.height()/2;

		circle.css({top: y+'px', left: x+'px'}).addClass("animate");
	},

	_handleClick(e) {
		this._doEffect(e);

		return this.props.onClick && this.props.onClick();
	},

	render: function() {
		var className = 'btn  btn-material ' + this.props.className;
		var type = this.props.type ? this.props.type : 'button';
		var style = this.props.style ? this.props.style : {};

		if (this.props.isLink) {
			return (
				<a
					href={this.props.href}
					className={className}
					onMouseDown={this._doEffect}
					onClick={this._handleClick}
					style={style}
				>
					<span>{this.props.text}</span>
				</a>
			);
		}

		return (
			<button
				type={type}
				className={className}
				onMouseDown={this._doEffect}
				onClick={this._handleClick}
			>
				<span>{this.props.text}</span>
			</button>
		);
	}

});

module.exports = Button;