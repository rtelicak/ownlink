var React = require('react');
var $ = require('jquery');
var LinkActions = require('../../actions/LinkActions');

var TwitterSharer = React.createClass({

	propTypes: {
		link: React.PropTypes.object.isRequired,
	},

	componentDidMount(){
		var w = twttr.widgets.createShareButton(
			this.props.link.fullUrl,
			document.getElementById('tweetAttachpoint'),
			{
				text: this.props.link.title
			}
		);

		var _this = this;

		twttr.events.bind(
			'tweet',
			function (event) {
				$.get('/admin/svc/link-shared/twitter/' + _this.props.link.id)
				.done(() =>{
					LinkActions.linkShared();
				})
				.fail(() => {
					console.log('twitter sharing failed');
				});
			}
		);
	},

	render: function() {
		return (
			<div>
				<div id="tweetAttachpoint"></div>
			</div>
		);
	}

});

module.exports = TwitterSharer;