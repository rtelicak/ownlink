var React = require('react');
var LinkActions = require('../../actions/LinkActions');

var FacebookSharer = React.createClass({

	propTypes: {
		link: React.PropTypes.object.isRequired,
	},

	handleClick(e){
		e.preventDefault();

		var redirectUrl = window.location.origin + '/admin/svc/link-shared/facebook/' + this.props.link.id;
		var app_id = window.location.host == 'ownlink.ly' ? '499467093555462' : '499464420222396';

		var facebookWindow = window.open(
			'https://www.facebook.com/dialog/share?' +
				'app_id=' + app_id +
				'&display=popup' +
				'&href=' + encodeURIComponent(this.props.link.fullUrl) +
				'&redirect_uri=' + redirectUrl,
			'',
			'width=575,height=436'
		);

		var interval = setInterval(function(){
			try {
				if (facebookWindow.location.host) {
					clearInterval(interval);
					LinkActions.linkShared();
					facebookWindow.close();
				}
			}
			catch (e) {}
		}, 1000);
	},

	render: function() {
		return (
			<div>
				<a className="facebook-sharer"
					onClick={this.handleClick}
					href="#">
					<i/>
					Share
				</a>
			</div>
		);
	}

});

module.exports = FacebookSharer;