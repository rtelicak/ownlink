var React = require('react');
var BrandColors = require('../utilities/brand-colors');

var ChartLegend = React.createClass({
	propTypes: {
		datasets: React.PropTypes.array.isRequired
	},

	render: function () {
		var datasets = this.props.datasets.map(function (ds, i) {
			return (
				<li key={i}>
					<span className="chart-legend__color-box" style={{ backgroundColor: ds.color }}></span>
					<span className="chart-legend__label capitalize">{ ds.label }</span>
				</li>);
		});

		return (
			<ul className="list-unstyled chart-legend">
			{ datasets }
			</ul>
		);
	}
});

module.exports = ChartLegend;