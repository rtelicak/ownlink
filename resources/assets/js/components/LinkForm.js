import React from 'react';
import assign from 'object-assign';
import Button from './_common/Button';
import LinkStore from '../stores/LinkStore';
import LinkActions from '../actions/LinkActions';
import connectToStores from '../utilities/connect-to-stores';
import { Dialog, TextField, RefreshIndicator } from 'material-ui';
import LinkDAO from '../api/LinkDAO';

var LinkForm = React.createClass({
	getInitialState() {
		return assign({}, this.props, {
			note: '',
			custom: '',
			original: '',
			formErrors: []
		});
	},

	handleSubmit: function(e) {
		e.preventDefault();

		var o = {
			note: this.state.note,
			custom: this.state.custom,
			original: this.state.original,
			domain_id: this.state.domainData.id,
			is_redirect: this.state.is_redirect,
		};

		LinkDAO.create(o,
		response => {
			this.closeDialog();
			this.setState({
				loading: false
			});

			LinkActions.createLinkCompleted(response);
		},
		err => {
			this.setState({
				formErrors: err.responseJSON,
				loading: false
			});
		});

		this.setState({
			loading: true
		});
	},

	closeDialog(){
		this.refs.dialog.dismiss();
	},

	_showDialog(){
		this.setState(this.getInitialState());
		this.refs.dialog.show();
	},

	onDialogShow(){
		setTimeout(() => {
			this.refs.original.focus();
		}, 500);
	},

	_handleChage(changedRef){
		return () => {
			this.setState({
				[changedRef]: this.refs[changedRef].getValue()
			});
		};
	},

	_showFormError(ref) {
		return this.state.formErrors[ref] ? this.state.formErrors[ref][0] : '';
	},

	render: function() {

		return (
			<div>
				<div className="tar">
					<Button
						className="btn-primary btn-raised"
						onClick={this._showDialog}
						text={'Create Redirect link'} />

					<Button
						className="btn-primary btn-raised"
						isLink ={true}
						href={'/admin/banner/create/' + this.state.domainData.id}
						style={{float: 'right'}}
						text={'Create Banner Link'} />
				</div>

				<Dialog
					ref="dialog"
					title="Create Redirect Link"
					onShow={this.onDialogShow}
					onDismiss={this.onDialogDismiss}
					autoScrollBodyContent={false} >

					<div className="dialog">
						<form onSubmit={this.handleSubmit} className="form-horizontal" role="search">
							<TextField
								ref='original'
								fullWidth={true}
								onChange={this._handleChage('original')}
								errorText={this._showFormError('original')}
								defaultValue={this.state.original}
								floatingLabelText="Enter a URL"/>

							<TextField
								ref='custom'
								fullWidth={true}
								onChange={this._handleChage('custom')}
								errorText={this._showFormError('custom')}
								defaultValue={this.state.custom}
								floatingLabelText={this._customRefLabelText()} />

							<TextField
								ref='note'
								onChange={this._handleChage('note')}
								fullWidth={true}
								defaultValue={this.state.note}
								floatingLabelText="Note"/>

							{this._renderButtons()}
						</form>
					</div>

				</Dialog>
			</div>
		);
	},

	_customRefLabelText(){
		if (! (this.state.custom && this.state.custom.length)) return 'Your Custom Link';

		return 'Your Custom Link (' + this.state.domainData.name + '/' + this.state.custom + ')';
	},

	_renderButtons(){
		if (this.state.loading) return (
			<div style={{
				position: 'relative',
				top: '10',
				width: '30',
				height: '42',
				margin: '0 auto'
			}}>
				<RefreshIndicator
					size={30}
					left={0}
					top={0}
					status="loading" />
			</div>);

		return (
			<div className="tar" style={{
				position: 'relative',
				top: '10',
			}}>
				<Button
					text="Cancel"
					className="btn-default"
					onClick={this.closeDialog}
					style={{marginRight: 15}} />
				<Button
					text="Create Redirect Link"
					className="btn-primary btn-raised"
					type='submit' />
			</div>
		);
	},
});

module.exports = connectToStores(LinkForm, [LinkStore], props => ({
	domainData: LinkStore.getDomainData(),
	is_redirect: 1,
	loading: false
}));