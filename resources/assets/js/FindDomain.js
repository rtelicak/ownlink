var React = require('react');
var FindForm = require('./components/FindDomain/FindForm');
var DomainSuggestion = require('./components/FindDomain/DomainSuggestion');

var FindDomain = React.createClass({

	getInitialState: function() {
		return {
			suggestions: [],
			domainCheckResult: '',
		};
	},

	showDomainCheckResult(domainCheckResult){
		this.setState({domainCheckResult});
	},

	setSuggestions: function(suggestions) {
		this.setState({suggestions, domainCheckResult: ''});
	},

	render: function() {
		var suggestions = this.state.suggestions && this.state.suggestions.map((suggestion, i) => {
			return <DomainSuggestion onDomainCheck={this.showDomainCheckResult} data={suggestion} key={i}/>;
		});

		var domainCheckResult = this.state.domainCheckResult;
		var domainCheckResult = domainCheckResult ? 
			<div>
				<div>Available: {domainCheckResult.isAvailable ? 'Yes' : 'No'}</div>
			</div>
			: '';

		return (
			<div>
				<FindForm onSuggestionsChange={this.setSuggestions}/>
				{domainCheckResult}
				{suggestions}
			</div>
		);
	}

});

React.render(<FindDomain />, document.getElementById('screen'));