import React from 'react';
import Links from './Link';
import history from './utilities/history';
import LinkDetail from './components/LinkDetail';
import Snackbar from './components/SnackbarMessages';
import { Router, Route, Link, IndexRoute } from 'react-router';

var mui = require('material-ui');
var ThemeManager = new mui.Styles.ThemeManager();


var About = React.createClass({

	render: function() {
		return (
			<div>About page</div>
		);
	}

});

var Admin = React.createClass({
	render: function() {
		return (
			<div>
				{this.props.children}
			</div>
		);
	}
});

var NoMatch = React.createClass({

	render: function() {
		return (
			<div>404 :(</div>
		);
	}

});
var Home = React.createClass({

	render: function() {
		return (
			<div>Home</div>
		);
	}

});


// have to wrap router to component to workaround `child context`
var Root = React.createClass({
	childContextTypes: {
		muiTheme: React.PropTypes.object
	},

	getChildContext: function() {
		return {
			muiTheme: ThemeManager.getCurrentTheme()
		};
	},

	render: function() {
		return (
			<span>
				<Router history={history}>
					<Route path="/" component={Admin}>
						<IndexRoute component={Links}/>
						<Route path="links" component={Links} >
							<Route path="/links/:linkId" component={LinkDetail} />
						</Route>
						<Route path="about" component={About}/>
					</Route>
				</Router>
				<Snackbar/>
			</span>
		);
	}

});

React.render(<Root />, document.getElementById('screen'));