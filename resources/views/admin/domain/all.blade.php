@extends('layouts.admin')

@section('title', 'Admin::dashboard')

@section('content')
	<div class="container">
		<h1>Your domains:</h1>

		@foreach($domains as $domain)
			<a href="{{ url('admin/domains', $domain->id)}}">
				<h4>{{ $domain->name }} ({{$domain->links->count()}} redirects)</h4>
			</a>
		@endforeach
	</div>

@endsection