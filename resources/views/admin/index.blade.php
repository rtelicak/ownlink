@extends('layouts.admin')

@section('title', 'Welcome to Admin')

@section('content')
<div class="container">
	<div id="screen"></div>
</div>

@stop

@section('scripts')
	<script src="/js/screen/twitter-widgets.js"></script>
	<script src="/js/admin.js"></script>
@stop