<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Create Call to action banner</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300' rel='stylesheet' type='text/css'>
	<style>
		<?= file_get_contents(base_path() . '/resources/views/banner/banner.css') ?>
		#ContentFrame {
			filter: blur(3px) grayscale(.8) opacity(0.8);
			-webkit-filter: blur(3px) grayscale(.8) opacity(0.8);
		}

	</style>
</head>
<body>
	<div class="container">
		<div id="screen"></div>
	</div>
	<script>
		window._domainData = JSON.parse('<?= json_encode($domainData) ?>');
	</script>
	<script src="/js/screen/banner.js"></script>
</body>
</html>
