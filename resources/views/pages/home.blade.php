@extends('layouts.public')

@section('title', 'Present your company with custom domain')

@section('content')

<div class="container">
	<div class="jumbotron">
		<h1>Hello, world!</h1>
		<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quae nulla incidunt quod? Pariatur modi vitae, inventore? Fugiat odit, consequuntur reprehenderit eaque at perspiciatis et quod cumque rerum, nostrum, esse itaque.</p>
		<p><a class="btn btn-primary btn-lg" href="{{ url('find-your-domain')}}" role="button">Find your domain</a></p>
	</div>
</div>

@stop