@extends('layouts.admin')

@section('title', 'Pay!')

@section('content')
<div class="container">
	<h1>You chose: {{$domain['name']}}.{{$domain['tld']}}</h1>

	<form action="" method="POST">
		<input type="hidden" name="_token" value="{{ csrf_token() }}">
		<button type="submit" class="btn btn-lg btn-primary">Order</button>
	</form>
</div>
@stop

