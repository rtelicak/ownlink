<style>
	<?= file_get_contents(base_path() . '/resources/views/banner/banner.css') ?>
</style>

<div class="link-banner">
	<div class="link-banner__brand">
		<a href="{{ $link->domain->config['redirectUrl'] }}">
			<img src='{{ $link->domain->config['brandImageUrl'] }}' width='50px' height='50px'/>
		</a>
		<a class="link-banner__brand__name" href='{{ $link->domain->config['redirectUrl'] }}'>
			{{ $link->domain->config['brandName'] }}
		</a>
	</div>

	<div class="link-banner__message_wrapper">
		<span class="link-banner__message">{{ $link->banner_message }}</span>
		<a id="collectConversion" class="link-banner__cta-button" target="_blank" href='{{ $link->button_target }}'>
			{{ $link->button_text }}
		</a>
	</div>

	<a class="link-banner__close" href='{{ $link->original }}'>&#215;</a>
</div>


@include('banner.jquery')
<script>
	// move this js to generated, to be able to redirect user clicking on CTA
	$(function(){
		var seconds = 0;
		var clickedConversionButton = false;

		var isTabActive = !window.top.document.hidden;

		window.top.onfocus = function () {
			// window.top.console.log('ACTIVE');
			isTabActive = true;
		};

		window.top.onblur = function () {
			// window.top.console.log('BLURRED');
			isTabActive = false;
		};

		setInterval(function(){
			isTabActive && seconds++;
			 // && window.top.console.log('counting ' + seconds);
		}, 1000);

		window.onbeforeunload = function(e) {
			if (clickedConversionButton) return;

			$.post('http://{{ $link->domain->name }}/svc/link-visit', {
				'conversion': 0,
				'link_id': "{{ $link->id}}",
				_token: "{{ csrf_token() }}",
				'time': seconds
			});
		};

		$('#collectConversion').on('click', function(e){
			clickedConversionButton = true;

			$.post('http://{{ $link->domain->name }}/svc/link-visit', {
				'conversion': 1,
				'link_id': "{{ $link->id}}",
				_token: "{{ csrf_token() }}",
				'time': seconds
			});
		});

		$('.link-banner__brand a').click(function(e){
			e.preventDefault();
			var brandUrl = $('.link-banner__brand a').first().attr('href');

			window.top.location.replace(brandUrl);
		})

		$('a.link-banner__close').click(function(e){
			e.preventDefault();
			var originalUrl = $('a.link-banner__close').first().attr('href');

			window.top.location.replace(originalUrl);
		})
	});

</script>