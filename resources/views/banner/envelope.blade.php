<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<?php print_r($htmlHead) ?>
	<style>
		<?= file_get_contents(base_path() . '/resources/views/banner/banner.css') ?>
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
</head>
<body>
	<iframe id="ContentFrame" src="{{ $link->original }}"></iframe>
	<iframe id="BannerFrame" src="http://{{ $link->domain->name }}/banner/{{ $link->id }}"></iframe>
</body>
</html>