@extends('layouts.public')

@section('title', 'Log in')

@section('content')

<div class="container">
	<form method="POST" action="/auth/login" class="form-horizontal">
		{!! csrf_field() !!}

		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
				<input name="email" type="email" class="form-control" id="email" placeholder="Email">
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input name="password" type="password" class="form-control" id="password" placeholder="Password">
			</div>
		</div>
		
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<div class="checkbox">
					<label>
						<input name="remember" type="checkbox"> Remember me
					</label>
				</div>
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Login</button>
			</div>
		</div>
	</form>
</div>
@endsection