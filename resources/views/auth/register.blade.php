@extends('layouts.public')

@section('title', 'Register in')

@section('content')
<div class="container">
	<form method="POST" action="/auth/register" class="form-horizontal">
		{!! csrf_field() !!}

		<div class="form-group">
			<label for="name" class="col-sm-2 control-label">Name</label>
			<div class="col-sm-10">
				<input name="name" type="input" class="form-control" id="name" placeholder="Name" value="{{ old('name') }}">
			</div>
		</div>

		<div class="form-group">
			<label for="email" class="col-sm-2 control-label">Email</label>
			<div class="col-sm-10">
				<input id="email" name="email" type="email" class="form-control" placeholder="Email" value="{{ old('email') }}">
			</div>
		</div>

		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Password</label>
			<div class="col-sm-10">
				<input id="password" name="password" type="password" class="form-control" placeholder="Password">
			</div>
		</div>

		<div class="form-group">
			<label for="password_confirmation" class="col-sm-2 control-label">Confirm password</label>
			<div class="col-sm-10">
				<input id="password_confirmation" name="password_confirmation" type="password" class="form-control" placeholder="Confirm password">
			</div>
		</div>

		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" class="btn btn-default">Register</button>
			</div>
		</div>
	</form>
</div>
@stop