<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>@yield('title')</title>
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

	@include('layouts._partials.public.nav')

	@yield('content')

	@if(count($errors))
	<div class="container">
		<div class="alert alert-danger">
			<a href="#" class="close" data-dismiss="alert">×</a>
			<ul class="list-unstyled">
				@foreach($errors->all() as $error)
					<li class="error">{{ $error }}</li>
				@endforeach
			</ul>
		</div>
	</div>
	@endif

	@include('layouts._partials.vendor-scripts')

	@yield('scripts')

</body>
</html>