<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>@yield('title')</title>
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,300' rel='stylesheet' type='text/css'>

	<link rel="stylesheet" href="/css/app.css">
</head>
<body>
	@include('layouts._partials.admin.nav')

	@yield('content')

	@include('layouts._partials.vendor-scripts')

	@yield('scripts')

</body>
</html>