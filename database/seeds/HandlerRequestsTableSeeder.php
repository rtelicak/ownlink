<?php

use Illuminate\Database\Seeder;

class HandlerRequestsTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		$links = App\Link::all();

		factory('App\Click', 1000)->make()->each(function($click) use($links){
			$links->random()->clicks()->create($click->getAttributes());
		});

	}
}
