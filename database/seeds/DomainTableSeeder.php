<?php

use Illuminate\Database\Seeder;

class DomainTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\Domain::truncate();
		$user = App\User::find(1);

		factory('App\Domain')->create([
			'name' => 'foo.bar',
			'user_id' => 1
		]);
	}
}
