<?php

use Illuminate\Database\Seeder;

class LinksTableSeeder extends Seeder
{
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		App\Click::truncate();
		App\Link::truncate();
		App\LinkStats::truncate();

		$domains = App\Domain::all();

		factory('App\Link', 40)->make()->each(function($link) use($domains){
			$link = $domains->random()->links()->create($link->getAttributes());
			$link->stats()->create(factory('App\LinkStats')->make()->getAttributes());
		});
	}
}
