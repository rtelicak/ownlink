<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RenameBannerVisitCountToBounceRate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('link_stats', function ($table) {
            $table->renameColumn('visit_count', 'bounce_count');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link_stats', function ($table) {
            $table->renameColumn('bounce_count', 'visit_count');
        });
    }
}
