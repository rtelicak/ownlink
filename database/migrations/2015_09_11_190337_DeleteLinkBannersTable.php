<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteLinkBannersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::drop('link_banners');

        Schema::table('links', function($table){
            $table->string('button_text');
            $table->string('button_target');
            $table->string('banner_message');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('link', function($table){
            $table->dropColumn(['button_title', 'button_target', 'banner_message']);
        });

        Schema::create('link_banners', function($table){
            $table->increments('id');
            $table->integer('link_id')->unsigned();
            $table->string('button_title');
            $table->string('button_target');
            $table->string('message');
            $table->timestamps();
        });
    }
}
