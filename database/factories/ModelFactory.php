<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
	return [
		'name' => $faker->name,
		'email' => $faker->email,
		'password' => bcrypt(str_random(10)),
		'remember_token' => str_random(10),
	];
});

$factory->define(App\Domain::class, function (Faker\Generator $faker) {
	return [
		'name' => strtolower($faker->domainName),
	];
});

$factory->define(App\Link::class, function (Faker\Generator $faker) {
	return [
		'from' => implode('-', $faker->words(2)),
		'to' => strtolower($faker->url),
		'title' => $faker->sentence(6),
		'note' => $faker->sentence(10),
		'is_redirect' => $faker->numberBetween(0,1),
		'created_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now')
	];
});

$factory->define(App\Click::class, function (Faker\Generator $faker) {
	return [
		'platform' => $faker->randomElement(['Windows', 'Windows NT', 'Mac OS X', 'Debian', 'Ubuntu', 'PPC', 'OpenBSD', 'Linux', 'CrOS']),
		'platform_version' => '123',
		'browser' => $faker->randomElement(['Opera', 'Chrome', 'Firefox', 'Safari', 'IE', 'Netscape', 'Mozilla']),
		'browser_version' => '123',
		'device' => $faker->randomElement(['iPhone', 'BlackBerry', 'HTC', 'Nexus', 'Dell', 'Motorola', 'Samsung', 'LG', 'Sony', 'Asus', 'Micromax', 'Palm', 'Vertu', 'Pantech', 'Fly', 'Wiko', 'iMobile', 'SimValley', 'Wolfgang', 'Alcatel', 'Nintendo', 'Amoi', 'INQ', 'GenericPhone']),
		'is_desktop' => $faker->randomElement([0,0,1]),
		'is_tablet' => $faker->randomElement([0,0,1]),
		'is_mobile' => $faker->randomElement([0,0,1]),
		'referer_url' => $faker->url(),
		'medium' => 'social',
		'source' => $faker->randomElement(['facebook', 'twitter', 'linkedIn', 'Google+']),
		'search_term' => '',
		'city' => $faker->city(),
		'country' => $faker->country(),
		'country_code' => $faker->countryCode(),
		'created_at' => $faker->dateTimeBetween($startDate = '-30 days', $endDate = 'now')
	];
});

$factory->define(App\LinkStats::class, function (Faker\Generator $faker) {
	$visit_count = $faker->numberBetween(100,300);

	return [
		'visit_count' => $visit_count,
		'time_spent_count' => $faker->numberBetween(1300,5000),
		'conversion_count' => $faker->numberBetween(40, $visit_count - 50),
	];
});